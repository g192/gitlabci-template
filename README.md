# GitLab CI/CD Terraform Pipeline

Use this pipeline to trigger Terraform builds in your project.

# Usage

## .gitlab-ci.yml

Create a `.gitlab-ci.yml` file in the root of your project and paste this content:
```yml
include: https://gitlab.com/krollege/gitlabci-template/raw/<version>/terraform.yml
```

## Project structure and tfvars
- Modularize your code (Check https://www.terraform.io/docs/configuration/modules.html for more information). 
- Set your terraform project with the following structure:
```
.
├── README.md
├── .gitlab-ci.yml
├── .gitignore
├── environments
│   ├── dev
│   │   ├── main.tf
│   │   └── variables.tf
│   ├── test
│   │   ├── main.tf
│   │   └── variables.tf
│   ├── stage
│   │   ├── main.tf
│   │   └── variables.tf
│   ├── prod
│   │   ├── main.tf
│   │   └── variables.tf
└── modules
    ├── child_module_1
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variable.tf
    ├── child_module_2
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    ├── main.tf
    └── variables.tf
```
Check https://www.terraform.io/docs/cloud/workspaces/repo-structure.html#directories

## CI/CI Variables
Some variables name have the `ENV` as prefix. This is for handling environments so variables with this prefix needs to be defined as environments you have.

| Variable  | Value |
| --------- | --------|
| APP_ID  | ID of your application. For example in mongodb repo this is `mongodb`, this is used as the bucket entry for state file so be carefull|
| GCP_STATE_BUCKET_KEY | GCP Credentials for GCS Bucket where the state file lives. |
| ${ENV NAME IN UPPERCASE}_STATEFILE_BUCKET_NAME | Name of the GCS Bucket where the state file lives. |
| ${ENV NAME IN UPPERCASE}_GCP_CREDENTIALS | GCP Credentials of the project you want to deploy stuff. |

### Kubernetes deployment
If you want to use this pipeline for Kubernetes deployments through terraform. Is required to specify the API Keys of the Rancher server in the environment. This is due to a network limitation that forces us to us Rancher as proxy for deployments to the GKE Cluster.

| Variable  | Value |
| --------- | --------|
|  ${ENV NAME IN UPPERCASE}_K8S_CLUSTER_NAME | Name of the cluster you want to deploy. |
|  ${ENV NAME IN UPPERCASE}_RANCHER_API_URL | url for the rancher api |
|  ${ENV NAME IN UPPERCASE}_RANCHER_ACCESS_KEY | access_key provided by rancher |
|  ${ENV NAME IN UPPERCASE}_RANCHER_SECRET_KEY | secret_key provided by rancher |

### Adding extra variables for injecting secrets in your Terraform code
You can add extra secrets like mongodb admin password or ssh keys through Gitlab CI/CD Variables. Just use the `EXTRA_VARS` variable in pipeline with a JSON array of strings with mappings of the name of the Gitlab variable to the terraform variable like `["<GITLAB VAR>:TF_VAR_<TERRAFORM VARIABLE>"]`. In your Gitlab CI/CD variables you can handle different values for each environment like `<ENV NAME IN UPPERCASE>_<VARIABLE NAME>`.


#### Example for MongoDB root user password:
##### Variables in Gitlab CI/CD
```
STAGE_MONGO_ADMIN_PASSWORD = password_for_stage
TEST_MONGO_ADMIN_PASSWORD = password_for_test
DEV_MONGO_ADMIN_PASSWORD = password_for_dev
```

##### Terraform code in the environment folder
`variables.tf`
```
variable "mongo_admin_password" {}
```

`main.tf`
```
module "mongodb" {
  source = "<path>"
  
  ...
  mongo_admin_password   = var.mongo_admin_password
  ...
}
```

##### EXTRA_VARS in your gitlab-ci.yml
`.gitlab-ci.yml`
```
include: https://gitlab.com/krollege/gitlabci-template/raw/master/terraform.yml

variables:
  EXTRA_VARS: |-
    [
      "MONGO_ADMIN_PASSWORD:TF_VAR_mongo_admin_password"
    ]
```

Explore MongoDB repo to see it in action https://gitlab.kroger.com/alcatraz/mongodb

### Override environments 
Pipeline provides the ENVIRONMENTS variable where you can specify the environments you only want to deploy. With this you avoid mistakes triggering stages in unwanted environments.

Example:
```
variables:
  EXTRA_VARS: |-
    [
      "MONGO_ADMIN_PASSWORD:TF_VAR_mongo_admin_password"
    ]
  ENVIRONMENTS: |-
    test
    stage
```
